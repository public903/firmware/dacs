#include <Arduino.h>
#include <idee.h>
#include <Ticker.h>
#include "board.h"
#include <switch.h>
#include <buzzer.h>

#define DEBUG

SW Button;
BUZZER Buzzer, Relay, Led;
uint8_t id[ID_LENGHT];
int16_t btn_ptr, bzr_ptr, rel_ptr, led_ptr;
MODE mode;

// Funkcie
void idStoreClear(void);
void toMasterMode(void);
void toWriteMode(void);
void toAccessMode(void);

// Create a timers
Ticker EeClearTmr(idStoreClear, 5000, 1, MILLIS);
Ticker MasterModeTmr(toAccessMode, 60000, 1, MILLIS);

//------------------------------------------------------------------
void setup() {
  Serial.begin(2400);
  Serial.setTimeout(10);
  while (!Serial);

  pinMode(DIP0_PIN,INPUT_PULLUP);
  pinMode(DIP1_PIN,INPUT_PULLUP);
  pinMode(DIP2_PIN,INPUT_PULLUP);

  delay(100);
  Button.begin(BTN_PIN, 1, &btn_ptr);
  Buzzer.begin(BUZZER_PIN, &bzr_ptr);
  Relay.begin(RELAY_PIN, &rel_ptr);
  Led.begin(LED_GRN_PIN, &led_ptr);

  // EEPROM Init
  if (eeInit()) {
    Buzzer.on(2000);
  };

  toAccessMode();
}
//------------------------------------------------------------------
void loop() {
  EeClearTmr.update();
  MasterModeTmr.update();
  Button.run();
  Buzzer.run();
  Relay.run();
  Led.run();

  /* Snimanie tlacitka */
  if (Button.getSwTrgOn()) {    /* MASTER mode */
    EeClearTmr.start();
    toMasterMode();
  } 
  if (Button.getSwTrgOff()) {   /* ACCESS mode */
    EeClearTmr.stop();
  }
    
  /* citanie karty z citacky */
  if (Serial.readBytes(id,PACKET_LENGHT) == PACKET_LENGHT) {  // bolo prijatych 5 bajtov z citacky
    switch (mode)
    {
    case master:
      writeIdMaster(id);
      Buzzer.on(200, 200, 3);
      toAccessMode();
      break;

    case write:
      /* Ak sa uz v pamati nachadza takato karta, tak ju vymaz */
      if (eraseId(id)) {  /* karta bola vymazana */
        Buzzer.on(100, 100, 2);
        toAccessMode();
      } else {  /* karta sa v pamati nenachadza - mozem zapisat */
        /* kontrola ci je v pamati miesto */
        uint16_t eeadr = checkEeFreeCell();
        if (eeadr) {  /* v pamati je miesto, mozem zapisovat */
          writeId(eeadr, id);
          Buzzer.on(500);
          toAccessMode();
        } else Buzzer.on(100, 100, 2);  /* v pamati neni miesto */
      }
      break;

    case access:
      Serial.println("Access mode: ");
      Serial.print("ID: ");
      for (uint8_t i=0; i<PACKET_LENGHT;i++) {
        Serial.print(id[i]); Serial.print(" ");
      }
      Serial.println();

      if (checkIdMaster(id)) {
        toWriteMode();
        break;
      } else if (checkId(id)) {
        if (digitalRead(DIP0_PIN)) Relay.on(RELAYPULSE1);
        else Relay.on(RELAYPULSE2);
        Buzzer.on(500);
      } else Buzzer.on(100, 100, 2);
      break;
    
    default:
      break;
    }
  }
}
//------------------------------------------------------------------
//------------------------------------------------------------------
void idStoreClear(void) {
  digitalWrite(LED_GRN_PIN, HIGH);
  Buzzer.on();
  eeClear();
  Buzzer.off();
  digitalWrite(LED_GRN_PIN, LOW);
  toAccessMode();
}

void toMasterMode(void) {
  mode = master;
  Buzzer.on(400, 100, 3);
  Led.on(50, 200, 240);
  MasterModeTmr.start();
}

void toWriteMode(void) {
  mode = write;
  Buzzer.on(400, 100, 2);
}

void toAccessMode(void) {
  mode = access;
  Led.on(500, 500, 0xffff);
  MasterModeTmr.stop();
}

