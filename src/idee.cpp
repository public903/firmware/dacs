#include <Arduino.h>
#include <EEPROM.h>
#include "idee.h"
#include "board.h"

bool eeInit(void) {
  bool eeInitFlag = true;
  for (uint8_t i=0; i < ID_LENGHT; i++) {
    if (EEPROM.read(i) != 0xff) eeInitFlag = false;
    #ifdef DEBUG
      Serial.println(EEPROM.read(i),HEX);
    #endif
  }
  if (eeInitFlag) {
    #ifdef DEBUG
      Serial.println("eeInitFlag");
    #endif
    eeClear();
    return(true);
  }
  return(false);
}

void eeClear(void) {
  for (uint16_t i=0; i<EEPROM.length(); i++) {
    EEPROM.write(i, 0);
  }
  return;
}

uint16_t checkId(uint8_t id[ID_LENGHT]) {
  for (uint16_t eeadr = 2 * ID_LENGHT; eeadr + ID_LENGHT < EEPROM.length(); eeadr += ID_LENGHT) {
    for (uint8_t i=0; i < ID_LENGHT; i++) {
      if (EEPROM.read(i+eeadr) != id[i]) i=100;
      else if (i == 4) return(eeadr);
    }
  }
  return 0;
}

uint16_t checkEeFreeCell(void) {
  uint8_t cell[ID_LENGHT] = {0xff,0xff,0xff,0xff,0xff};
  uint16_t cellAdr = checkId(cell);
  if (cellAdr) return(cellAdr);
  else {
    for (uint8_t i=0; i<ID_LENGHT; i++) {
      cell[i] = 0;
    }
    cellAdr = checkId(cell);
    if (cellAdr) return(cellAdr);
    else return(0);
  }
}

uint16_t checkIdMaster(uint8_t id[ID_LENGHT]) {
  uint16_t eeadr = ID_LENGHT;
    for (uint8_t i=0; i < ID_LENGHT; i++) {
      if (EEPROM.read(i+eeadr) != id[i]) i=100;
      else if (i == 4) return(eeadr);
    }
  return 0;
}

bool writeIdMaster(uint8_t id[ID_LENGHT]) {
  for (uint8_t i=ID_LENGHT; i < ID_LENGHT + ID_LENGHT; i++) {
    Serial.print("i: "); Serial.print(i);
    Serial.print("  id[i]: ");Serial.println(id[i-ID_LENGHT]);
    EEPROM.write(i, id[i-ID_LENGHT]);
  }
  return(true);
}

bool writeId(uint16_t eeAdr, uint8_t id[ID_LENGHT]) {
  for (uint8_t i=eeAdr; i < eeAdr + ID_LENGHT; i++) EEPROM.write(i, id[i-eeAdr]);
  return(true);
}

uint16_t eraseId(uint8_t id[ID_LENGHT]) {
  uint16_t eeadr = checkId(id);
  if (eeadr) {
    for (uint16_t i = eeadr; i < eeadr + ID_LENGHT; i++) EEPROM.write(i, 0xff);
    return(eeadr);
  } else return(0);
}
