/*
 * board.h
 *
 * Created: 13. 4. 2022
 *  Author: Jan Trnik
 */ 


#ifndef BOARD_H_
#define BOARD_H_

#define RELAYPULSE1     500
#define RELAYPULSE2     3000

// Pins
#define LED_GRN_PIN     13
#define RELAY_PIN       8
#define BTN_PIN         A1
#define INPUT2_PIN      A0
#define DIP0_PIN        5
#define DIP1_PIN        6
#define DIP2_PIN        7
#define BUZZER_PIN      INPUT2_PIN

#define PACKET_LENGHT   5

enum MODE {
  master,
  write,
  access
};
//---------------------------------------------------------------------------


#endif /* BOARD_H_ */