/*
 * idee.h
 *
 * Created: 15. 4. 2022
 *  Author: Jan Trnik
 * 
 * IDEE - ID EEPROM
 * práca s eeprom pamäťou pre ukladanie hladanie a čítanie ID čísel čipových kariet
 */ 


#ifndef IDEE_H_
#define IDEE_H_

#include <Arduino.h>
#include <EEPROM.h>
#include "board.h"

#define ID_LENGHT   5
#define EELED_PIN     LED_GRN_PIN  

//---------------------------------------------------------------------------
bool eeInit(void);
void eeClear(void);
uint16_t checkId(uint8_t id[ID_LENGHT]); 
uint16_t checkEeFreeCell(void);
uint16_t checkIdMaster(uint8_t id[ID_LENGHT]);
bool writeIdMaster(uint8_t id[ID_LENGHT]);
bool writeId(uint16_t eeAdr, uint8_t id[ID_LENGHT]);
uint16_t eraseId(uint8_t id[ID_LENGHT]);

#endif /* IDEE_H_ */