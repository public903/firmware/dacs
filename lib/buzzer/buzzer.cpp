#include <Arduino.h>
#include <buzzer.h>

/***********************************/
/************ BUZZER **************/
/***********************************/
BUZZER::BUZZER() {
}
/***********************************/
void BUZZER::begin(uint8_t pin, int16_t* val) {
  ptrVal = val;
  pinBuzz = pin;
  pinMode(pinBuzz,OUTPUT);
  *ptrVal = 0;
  value = *ptrVal;
  digitalWrite(pinBuzz, *ptrVal);
}  
/***********************************/
void BUZZER::on(void) {
  *ptrVal = 1;
  value = *ptrVal;
  digitalWrite(pinBuzz, *ptrVal);
  tmrOn = 0;
}  
/***********************************/
void BUZZER::on(uint16_t tOn) {
  *ptrVal = 1;
  value = *ptrVal;
  digitalWrite(pinBuzz, *ptrVal);
  tmrOn = tOn;
  millisOld = millis();
}  
/***********************************/
void BUZZER::on(uint16_t tOn, uint16_t tOff, uint16_t num) {
  *ptrVal = 1;
  value = *ptrVal;
  digitalWrite(pinBuzz, *ptrVal);
  tmrOn = tOn;
  tmrOff = tOff;
  numOfRep = num;
  millisOld = millis();
} 
/***********************************/
void BUZZER::off(void) {
  *ptrVal = 0;
  tmrOn = 0;
  value = *ptrVal;
  digitalWrite(pinBuzz, *ptrVal);
}  
/***********************************/
bool BUZZER::run(void) {
  if (*ptrVal != value) {    // synchro
    tmrOn = 0;
    value = *ptrVal;
    digitalWrite(pinBuzz, *ptrVal);
  }

  if (*ptrVal && tmrOn && ((uint16_t)((uint16_t)millis() - millisOld) >= tmrOn)) {    // buzzer off
    millisOld = millis();
    *ptrVal = 0;
    value = *ptrVal;
    digitalWrite(pinBuzz, *ptrVal);
    if (numOfRep && (numOfRep != 0xffff)) numOfRep--;
  }

  if (!*ptrVal && numOfRep && ((uint16_t)((uint16_t)millis() - millisOld) >= tmrOff)) {    // buzzer on
    millisOld = millis();
    *ptrVal = 1;
    value = *ptrVal;
    digitalWrite(pinBuzz, *ptrVal);
  }

  return(*ptrVal);
}
