#ifndef buzzer_h
#define buzzer_h

#include <Arduino.h>

class BUZZER {
public:
  BUZZER();
  void begin(uint8_t pin, int16_t* val);  // pin - pripojenie ledky; pointer na stav ledky
  bool run(void);           // obsluha buzzera + vrati aktualny stav buzzera
  void on(void);    // zapni buzzer na
  void on(uint16_t tOn);    // zapni buzzer na cas time. Ak je time=0 zapne na stalo
  void on(uint16_t tOn, uint16_t tOff, uint16_t num); // blikanie> tOn-ms; tOff-ms; num-pocet pipnuti
  void off(void);   // vypni buzzer

private:
  uint8_t pinBuzz;
  int16_t* ptrVal, value;
  uint16_t millisOld, tmrOn, tmrOff, numOfRep;
};

#endif
