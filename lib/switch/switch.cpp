#include <Arduino.h>
#include <switch.h>

SW::SW() {
}
/***********************************/
void SW::begin(uint8_t pin, uint8_t inverse, int16_t *val) {
  pinSw = pin;
  pinInverse = inverse;
  ptrVal = val;
  msDebounce = 0;
  msTime = millis();
  time = 0;
  pinMode(pinSw, INPUT_PULLUP);
  uint8_t stateNow = digitalRead(pinSw);
  if (pinInverse) *ptrVal = !stateNow;
  else *ptrVal = stateNow;
  stateOld = stateNow;
  tglOff = 0; tglOn = 0;  
}
/***********************************/
bool SW::run(void) {
  uint8_t stateNow = digitalRead(pinSw);

  if (stateNow != stateOld) {
    msDebounce = (uint8_t)millis();
    if (!msDebounce) msDebounce--;
    stateOld = stateNow;
  }

  if (msDebounce && (uint8_t)((uint8_t)millis() - msDebounce) >= DEBOUNCE) {
    msDebounce = 0;
    if (pinInverse) *ptrVal = !stateNow;
    else *ptrVal = stateNow;

    if (*ptrVal) {
      tglOn = 1; tglOff = 0;
    }
    else {
      tglOn = 0; tglOff = 1;
    }
    time = 0; timeExec = 0;
  }

  if (uint8_t msTemp = (uint8_t)((uint8_t)millis() - msTime) >= 1) {
    msTime = (uint8_t)millis();
    time += msTemp;
  }

  return(*ptrVal);
}   
/***********************************/
bool SW::getSwTrgOn(void) {
  if (tglOn) {
    tglOn = 0;
    return(true);
  }
  return(false);
}   
/***********************************/
bool SW::getSwTrgOff(void) {
  if (tglOff) {
    tglOff = 0;
    return(true);
  }
  return(false);
}   
/***********************************/
bool SW::getSwState(void) {
  return(*ptrVal);
} 
/***********************************/
uint32_t SW::getSwTime(void) {
  return(time);
} 
/***********************************/
bool SW::getSwTimeOver(uint32_t timeOver) {
  if (!timeExec && (time >= timeOver)) {
    timeExec = 1;
    return(true);
  }
  return(false);
} 
