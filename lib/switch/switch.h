#ifndef switch_h
#define switch_h

#include <Arduino.h>

#define DEBOUNCE 20

class SW {
public:
  SW();
  void begin(uint8_t pin, uint8_t inverse, int16_t *val);  // pin - pripojenie spinaca; mode - vypinac 0 alebo tlacitko 1; pointer na stav spinaca
  bool run(void);           // vrati aktualny stav spinaca, tzn On / Off
  bool getSwTrgOn(void);    // ak doslo k zapnutiu, vrati TRUE
  bool getSwTrgOff(void);   // ak doslo k vypnutiu spinaca, vrati TRUE
  bool getSwState(void);    // vrati stav spinaca
  uint32_t getSwTime(void); // vrati pocet milisekund ktore je sw v aktualnom stave
  bool getSwTimeOver(uint32_t timeOver);  // vrati 1 ak pretiekol zadany cas spinaca

private:
  uint8_t pinSw, pinInverse, msTime, stateOld, msDebounce, tglOn, tglOff, timeExec;
  int16_t *ptrVal;
  uint32_t time;
};

#endif