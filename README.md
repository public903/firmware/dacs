# DACS
## Door Access Control System

### Popis:
Firmware jednodverového prístupového systému pre čítačku RFID EM.  
Kapacita pamäti: 200 kariet

DIP1 Off - relátkový impulz je 500ms; DIP1 On - relátkový impulz je 3000ms

Vstupné dáta: surové dáta, tj 5 bytov, čo je dĺžka ID čipovej karty EM

**Funkcie:**

- tlačítkom sa nastaví mód programovania MASTER karty
- priložením MASTER karty sa nastaví mód pridávania / mazania prístupovej karty:
  - prilož MASTER kartu
  - prilož náramok, ktorý chceš pridať alebo vymazať. Program si najprv zistí či sa daná karta nachádza v pamäti a ak sa nenachádza tak pridá kartu do pamäti. Ak sa v pamäti nachádza, tak ju vymaže.
- podržaním tlačítka 5s, sa úplne vymaže a naformátuje pamäť EEPROM, kde sú uložené všetky prístupové karty, ako aj MASTER karta. Po tomto úkone sa ozve dlhé pípnutie. Potom treba nanovo naprogramovať MASTER kartu a prístupové karty.

### Hardware
Arduino NANO 328

PRESEN-FSR (jatel.sk)

### Pripojenie:
> LED	D13 (nepripája sa, je na doske)  
> Tlačítko	A1  
> Relé		D8  
> Buzzer	A0  
>
> DIP0,1,2	D5, D6, D7
